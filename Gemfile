# frozen_string_literal: true

source 'https://rubygems.org'

ruby '2.6.2'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails'

# Use SCSS for stylesheets
gem 'sass-rails'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
# gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', group: :doc

gem 'annotate'
gem 'bootstrap-sass'
gem 'client_side_validations'
gem 'client_side_validations-simple_form'
gem 'cocoon'
gem 'devise'
gem 'devise-i18n'
gem 'font-awesome-rails'
gem 'haml'
gem 'high_voltage'
gem 'i18n-js'
gem 'pg'
gem 'pundit'
gem 'reject_deeply_nested'
gem 'simple_form'
gem 'sorted-activerecord'
gem 'sprockets-rails', '2.3.3'
# Rich text editor
gem 'trix-rails', require: 'trix'
gem 'webdrivers'
gem 'webpack'

group :development, :test do
  gem 'brakeman', require: false
  gem 'byebug'
  gem 'connection_pool'
  gem 'launchy'
  gem 'orderly'
  gem 'pry-byebug'
  gem 'rails-controller-testing'
  gem 'rb-readline'
  gem 'rspec-rails'
  gem 'simplecov', require: false
  # Page objects
  gem 'site_prism'
end

group :development do
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'bundler-audit'
  gem 'erb2haml'
  gem 'fasterer'
  gem 'i18n-tasks'
  gem 'overcommit'
  gem 'rails-erd'
  gem 'rails_best_practices'
  # Spring speeds up development by keeping your application running in the
  # background.
  # Read more: https://github.com/rails/spring  gem 'spring'
  # gem 'i18n-debug'
  # Access an IRB console on exactly().timesception pages or
  # by using <%= console %> in views
  # Reek checks ruby code for errors
  gem 'reek'
  gem 'rubocop'
  gem 'rubocop-performance'
  gem 'rubocop-rspec'
  gem 'web-console'
  # yard is used to document code with comments about return types and
  # parameters before each method
  gem 'yard'
end

group :test do
  gem 'capybara'
  gem 'capybara-selenium'
  gem 'cucumber'
  gem 'cucumber-rails', require: false
  gem 'database_cleaner'
  gem 'factory_bot_rails'
  gem 'selenium-webdriver'
end

group :production do
  gem 'newrelic_rpm'
  gem 'puma'
  gem 'rails_12factor'
end
